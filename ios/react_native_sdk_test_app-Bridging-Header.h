//
//  react_native_sdk_test_app-Bridging-Header.h
//  react_native_sdk_test_app
//
//  Created by appgain on 17/08/2023.
//

#import <React/RCTBridgeModule.h>
#import <Appgain/Appgain.h>

@interface RCTCalendarModule : NSObject <RCTBridgeModule>
@end
