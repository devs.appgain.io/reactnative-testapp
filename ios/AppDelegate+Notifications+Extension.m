//
//  AppDelegate+Notifications+Extension.m
//  react_native_sdk_test_app
//
//  Created by appgain on 17/08/2023.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(extensions, NSObject);

RCT_EXTERN_METHOD(registerForRemoteNotification);
RCT_EXTERN_METHOD(application( didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) (RCTResponseSenderBlock)callback);

@end

  
