//
//  AppDelegate+Extension.swift
//  PragueNow
//
//  Created by Ragaie Alfy on 8/24/19.
//  Copyright © 2019 Ragaie Alfy. All rights reserved.
//

import Foundation
import UIKit

import UserNotificationsUI
import UserNotifications
//import Appgain

@objc(extensions)
class extensions :NSObject , UIApplicationDelegate {
  @objc
    func  registerForRemoteNotification(){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
          let center = UNUserNotificationCenter.current()
          center.delegate = self as? UNUserNotificationCenterDelegate
            var authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          if #available(iOS 12.0, *) {
            authOptions = UNAuthorizationOptions(rawValue: authOptions.rawValue | UNAuthorizationOptions.provisional.rawValue)
          }
          center.requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
          
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            DispatchQueue.main.async {
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
        }
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
  
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    print("Successfully registered for notifications!")
    let tokens = deviceToken.reduce("") { $0 + String(format: "%02x", $1) }
            print("device token: \(tokens)")
          print("DEVICE TOKEN = \(deviceToken)")
          print("Device token string ==> \(deviceToken.base64EncodedString())")
          Appgain.registerDevice(withToken: deviceToken)
          let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
          let token = tokenParts.joined()
          print("Device Token: \(token)")
        
      }
  
  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to register for notifications: \(error.localizedDescription)")
  }
  @objc
     func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let url = response.notification.request.content.userInfo["url"] as? String{
            switch UIApplication.shared.applicationState {
            case .background, .inactive:
                    // background
                
                let deadlineTime = DispatchTime.now() + .seconds(8)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    print("test")
                
                if  let urll = URL(string: url){
            
                    UIApplication.shared.open(urll) { (result) in
                        if result {
                           // The URL was delivered successfully!
                        }
                    }
                }
            }
                break
                case .active:
                    // foreground
                    if  let urll = URL(string: url){
                
                        UIApplication.shared.open(urll) { (result) in
                            if result {
                               // The URL was delivered successfully!
                            }
                        }
                    }

                    break
                default:
                    break
            }
            
        }

        
        Appgain.recordPushStatus(NotificationStatus.opened(), userInfo: response.notification.request.content.userInfo)
//        if Auth.auth().canHandleNotification(response.notification.request.content.userInfo) {
//            completionHandler()
//            return
//        }
    }
  @objc
     func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        completionHandler([.alert, .badge, .sound])
        Appgain.recordPushStatus(NotificationStatus.conversion(), userInfo: notification.request.content.userInfo)

    }
  @objc
     func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let url = userInfo["url"] as? String{
            switch UIApplication.shared.applicationState {
            case .background, .inactive:
                    // background
                
                let deadlineTime = DispatchTime.now() + .seconds(8)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    print("test")
                
                if  let urll = URL(string: url){
            
                    UIApplication.shared.open(urll) { (result) in
                        if result {
                           // The URL was delivered successfully!
                        }
                    }
                }
            }
                break
                case .active:
                    // foreground
                    if  let urll = URL(string: url){
                
                        UIApplication.shared.open(urll) { (result) in
                            if result {
                               // The URL was delivered successfully!
                            }
                        }
                    }

                    break
                default:
                    break
            }
            
        }
        Appgain.handlePush(userInfo, for: application)
        
    }
   
  @objc
     func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        //silent notification for update content
      
        Appgain.handlePush(userInfo, for: application)
    }
    


}
