#import "AppDelegate.h"
#import <Firebase.h>
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <Appgain/Appgain.h>

#ifdef FB_SONARKIT_ENABLED
#import <FlipperKit/FlipperClient.h>
#import <FlipperKitLayoutPlugin/FlipperKitLayoutPlugin.h>
#import <FlipperKitUserDefaultsPlugin/FKUserDefaultsPlugin.h>
#import <FlipperKitNetworkPlugin/FlipperKitNetworkPlugin.h>
#import <SKIOSNetworkPlugin/SKIOSNetworkAdapter.h>
#import <FlipperKitReactPlugin/FlipperKitReactPlugin.h>

static void InitializeFlipper(UIApplication *application) {
  FlipperClient *client = [FlipperClient sharedClient];
  SKDescriptorMapper *layoutDescriptorMapper = [[SKDescriptorMapper alloc] initWithDefaults];
  [client addPlugin:[[FlipperKitLayoutPlugin alloc] initWithRootNode:application withDescriptorMapper:layoutDescriptorMapper]];
  [client addPlugin:[[FKUserDefaultsPlugin alloc] initWithSuiteName:nil]];
  [client addPlugin:[FlipperKitReactPlugin new]];
  [client addPlugin:[[FlipperKitNetworkPlugin alloc] initWithNetworkAdapter:[SKIOSNetworkAdapter new]]];
  [client start];
}
#endif

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

#ifdef FB_SONARKIT_ENABLED
  InitializeFlipper(application);
  
#endif
  [FIRApp configure];
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"react_native_sdk_test_app"
                                            initialProperties:nil];

  if (@available(iOS 13.0, *)) {
      rootView.backgroundColor = [UIColor systemBackgroundColor];
  } else {
      rootView.backgroundColor = [UIColor whiteColor];
  }

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
- (void)registerforremotenotification {
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = (id<UNUserNotificationCenterDelegate>)self;

        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound;
        if (@available(iOS 12.0, *)) {
            authOptions = authOptions | UNAuthorizationOptionProvisional;
        }

        [center requestAuthorizationWithOptions:authOptions
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {}];
    } else {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound
                                                                                 categories:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        });
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    });
}
// iOS 7 or iOS 6
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Successfully registered for notifications!");
    
    const unsigned char *bytes = [deviceToken bytes];
    NSMutableString *hexString = [NSMutableString new];
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [hexString appendFormat:@"%02x", bytes[i]];
    }
    NSLog(@"Device token: %@", hexString);
    
    NSString *tokenString = [NSString stringWithUTF8String:[deviceToken bytes]];
    NSLog(@"Device token = %@", deviceToken);
    NSLog(@"Device token string ==> %@", [deviceToken base64EncodedStringWithOptions:0]);
  [Appgain RegisterDeviceWithToken:deviceToken];
    NSMutableArray<NSString *> *tokenParts = [NSMutableArray new];
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [tokenParts addObject:[NSString stringWithFormat:@"%02.2hhx", bytes[i]]];
    }
    NSString *token = [tokenParts componentsJoinedByString:@""];
    NSLog(@"Device token: %@", token);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
   didReceiveNotificationResponse:(UNNotificationResponse *)response
            withCompletionHandler:(void (^)(void))completionHandler {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    NSString *urlString = userInfo[@"url"];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground || [UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        dispatch_time_t deadlineTime = dispatch_time(DISPATCH_TIME_NOW, 8 * NSEC_PER_SEC);
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        
        dispatch_after(deadlineTime, mainQueue, ^(void){
            NSLog(@"test");
            
            NSURL *url = [NSURL URLWithString:urlString];
            if (url) {
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
                    if (success) {
                        // the URL was delivered successfully!
                    }
                }];
            }
        });
    } else if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        NSURL *url = [NSURL URLWithString:urlString];
        if (url) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    // the URL was delivered successfully!
                }
            }];
        }
    }
    
//    [appgain recordPushStatus:[NotificationStatus opened] userinfo:response.notification.request.content.userInfo];
    //    if ([Auth.auth canHandleNotification:response.notification.request.content.userInfo]) {
    //        completionHandler();
    //        return;
    //    }
    
//    completionHandler();
}
- (void)usernotificationcenter:(UNUserNotificationCenter *)center willpresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionhandler {
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    completionhandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
    [Appgain recordPushStatus:[notification.request.content.userInfo objectForKey:@"conversion"] userInfo:notification.request.content.userInfo];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {

    application.applicationIconBadgeNumber = 0;
    
    if (userInfo[@"url"]) {
        NSString *urlString = userInfo[@"url"];
        
        switch (application.applicationState) {
            case UIApplicationStateBackground:
            case UIApplicationStateInactive: {
                dispatch_time_t deadlineTime = dispatch_time(DISPATCH_TIME_NOW, 8 * NSEC_PER_SEC);
                dispatch_queue_t mainQueue = dispatch_get_main_queue();
                dispatch_after(deadlineTime, mainQueue, ^{
                    NSLog(@"test");
                    
                    NSURL *url = [NSURL URLWithString:urlString];
                    if (url) {
                        [UIApplication.sharedApplication openURL:url options:@{} completionHandler:^(BOOL success) {
                            if (success) {
                                // The URL was delivered successfully!
                            }
                        }];
                    }
                });
                break;
            }
            case UIApplicationStateActive: {
                NSURL *url = [NSURL URLWithString:urlString];
                if (url) {
                    [UIApplication.sharedApplication openURL:url options:@{} completionHandler:^(BOOL success) {
                        if (success) {
                            // The URL was delivered successfully!
                        }
                    }];
                }
                break;
            }
            default:
                break;
        }
    }
    
    [Appgain handlePush:userInfo forApplication:application];
    
    completionHandler(UIBackgroundFetchResultNewData);
  [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
  //silent notification for update content
//[Appgain handlePush:userInfo for:application];
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    //silent notification for update content
//  [Appgain handlePush:userInfo for:application];
//}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index"];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

@end
