/* eslint-disable no-undef */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import * as React from 'react';
import { useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  Button,
  Platform,
  NativeModules,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { useState } from 'react';

import AppgainSDK from 'react-native-appgain-sdk-library';
import { PermissionsAndroid, TextInput } from 'react-native';
import Prompt from 'react-native-prompt';

//import AppgainSDK from 'dummy-sdk-test-project'

import {
  setNativeExceptionHandler,
  setJSExceptionHandler,
  getJSExceptionHandler,
} from 'react-native-exception-handler';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';
import { getDeviceToken, syncUniqueId } from 'react-native-device-info';
const APP_ID = '';
const APP_API_KEY = '';
const LOG_EVENT = {
  type: 'EVENT_TYPE',
  action: 'EVENT_Action',
  extrasKey: 'extra Key',
  extrasValue: 'extra Value',
};
const EMAIL = {
  updateUserEmail: '',
  updateUserPhone: '',
  updateUserUpdatedField1: '',
  updateUserUpdatedField2: '',
}

export async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Example App',
        message: 'Example App access to your location ',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the location');
      //  alert("You can use the location");
    } else {
      console.log('location permission denied');
      // alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

setJSExceptionHandler((error, isFatal) => {
  Alert.alert('JS Error!!', error[{ text: 'Ok', onPress: () => { } }], {
    cancelable: false,
  });
});

setNativeExceptionHandler(exceptionString => {
  Alert.alert(
    'Native Error!!',
    exceptionString[{ text: 'Ok', onPress: () => { } }],
    { cancelable: false },
  );
});

// console.log(
//   'hello',
//   Object.getOwnPropertyNames(AppgainSDK).filter(function (x) {
//     return typeof AppgainSDK[x] === 'function';
//   }),
// );

// Alert.alert(
//   'AppgainSDK',
//   JSON.stringify(
//     Object.getOwnPropertyNames(AppgainSDK).filter(function (x) {
//       return typeof AppgainSDK[x] === 'function'
//     }), { cancelable: false }
//   ))
// const { NotificationBridge } = NativeModules;
const App: () => React$Node = () => {
  useEffect(() => {
    if (Platform.OS === 'android') {
      requestLocationPermission();
    }
    initNotification();
  }, []);

  const initNotification = async () => {
    firebase.initializeApp();
    try {
      await messaging().requestPermission();


    } catch (error) {
      console.log('Error in retrieving device token:', error);
    }
  };

  const getTokenUpdateUserToken = async () => {
    const enabled = await messaging().hasPermission();
    // console.log(enabled)
    if (enabled) {
      messaging()
        .getAPNSToken()
        .then(async apns => {
          await AppgainSDK.updateUserData({
            deviceToken: apns,
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else {
      initNotification();
    }
  }

  const [isFireAutomatorDialogVisible, updateIsFireAutomatorDialogVisible] =
    useState(false);
  const [
    isFireAutomatorWithPersonlaiztionDialogVisible,
    updateIsFireAutomatorWithPersonlaiztionDialogVisible,
  ] = useState(false);
  const [isPurchaseDialogVisible, updateIsPurchaseDialogVisible] =
    useState(false);
  const [isEmailDialogVisible, updateIsEmailDialogVisible] = useState(false);
  const [isSmsDialogVisible, updateIsSmsDialogVisible] = useState(false);
  const [isSetUserDialogVisible, updateIsSetUserIdDialogVisible] =
    useState(false);
  const [isLogEventDialogVisible, updateLogEventDialogVisible] =
    useState(false);
  const [isInitUserDialogVisible, updateInitUserDialogVisible] =
    useState(false);
  const [isUpdatetUserDialogVisible, updateUpdateUserDialogVisible] =
    useState(false);
  const [isStartAlertDialogVisible, updateStartAlertDialogVisible] =
    useState(true);
  const [firstInput, updateFirstInput] = useState(APP_ID);
  const [secondInput, updateSecondInput] = useState(APP_API_KEY);

  let isInitSDk = true;
  let automatorText;
  let automatorWithPersText;
  let purchaseText;
  let emailText;
  let smsText;

  fireAutomatorShowDialog = isShow => {
    updateIsFireAutomatorDialogVisible(isShow);
  };
  startAlertShowDialog = isShow => {
    updateStartAlertDialogVisible(isShow);
  };
  fireAutomatorWithPersonlaiztionShowDialog = isShow => {
    updateIsFireAutomatorWithPersonlaiztionDialogVisible(isShow);
  };
  purchaseShowDialog = isShow => {
    updateIsPurchaseDialogVisible(isShow);
  };
  emailTextShowDialog = isShow => {
    updateIsEmailDialogVisible(isShow);
  };
  smsTextShowDialog = isShow => {
    updateIsSmsDialogVisible(isShow);
  };
  setUserIdTextShowDialog = isShow => {
    updateIsSetUserIdDialogVisible(isShow);
  };
  setLogEventTextShowDialog = isShow => {
    updateLogEventDialogVisible(isShow);
  };
  setInitUserTextShowDialog = isShow => {
    updateInitUserDialogVisible(isShow);
  };

  setUpdateUserTextShowDialog = isShow => {
    updateUpdateUserDialogVisible(isShow);
  };
  sendFireAutomatorInput = async inputText => {
    automatorText = inputText;
    this.fireAutomatorShowDialog(false);
    try {
      await AppgainSDK.fireAutomator({ triggerPointName: automatorText });
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };

  sendAutomatorWithProsInput = async (
    inputText1,
    fireAutomatorKey,
    fireAutomatorValue,
  ) => {
    automatorWithPersText = inputText1;
    this.fireAutomatorWithPersonlaiztionShowDialog(false);
    var payload = {};

    payload[`${fireAutomatorKey}`] = fireAutomatorValue;
    try {
      await AppgainSDK.fireAutomator({
        triggerPointName: automatorWithPersText,
        payload: payload,
      });
      console.log(automatorWithPersText);
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);

      Alert.alert('Error: ', error);
    }
  };

  sendPurchaseInput = async (productName, currency, amount) => {
    try {
      await AppgainSDK.addPurchase(productName, currency, parseInt(amount));
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };

  sendEmailInput = async inputText3 => {
    emailText = inputText3;
    this.emailTextShowDialog(false);
    try {
      await AppgainSDK.addNotificationChannel('email', emailText);
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };

  sendSmsInput = async inputText4 => {
    smsText = inputText4;
    this.smsTextShowDialog(false);
    try {
      await AppgainSDK.addNotificationChannel('SMS', inputText4);
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };
  sendUserIdInput = async inputText5 => {
    try {
      await AppgainSDK.setUserId(inputText5);
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };
  sendLogEventInput = async (
    logEventAction,
    logEventType,
    logEventExtrasKey,
    logEventExtrasValue,
  ) => {
    var extras = {};
    extras[`${logEventExtrasKey}`] = logEventExtrasValue;
    try {
      await AppgainSDK.logEvent(logEventAction, logEventType, extras);
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };
  sendInitUserInput = async (email, password, username) => {
    Alert.alert(email + ' ' + password + ' ' + username);
    try {
      let initUserReuslt = await AppgainSDK.initUser(email, password, username);
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };

  // eslint-disable-next-line no-undef
  sendUpdateUserInput = async (
    updateUserEmail,
    updateUserPhone,
    updateUserUpdatedField1,
    updateUserUpdatedField2,
  ) => {
    try {
      await AppgainSDK.updateUserData({
        userEmail: updateUserEmail,
        phone: updateUserPhone,
        updatedField1: updateUserUpdatedField1,
        updatedField2: updateUserUpdatedField2,
      });
      Alert.alert('Success');
    } catch (error) {
      console.log('Error: ', error);
      Alert.alert('Error: ', error);
    }
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          style={
            {
              // flex: 10,
              // flexDirection: 'column',
              // justifyContent: 'center',
              // alignItems: 'stretch',
            }
          }>
          <View style={styles.btn_cont}>
            <Prompt
              title="Init Sdk"
              placeholder="APP ID"
              defaultValue=""
              numberOfTextIntput={2}
              visible={isStartAlertDialogVisible}
              onCancel={() => this.startAlertShowDialog(false)}
              onSubmit={(first, second) => {
                updateFirstInput(first);
                updateSecondInput(second);
                this.startAlertShowDialog(false);
              }}
            />
            <Button
              nativeID="initBtn"
              style={styles.btn}
              onPress={async () => {
                try {
                  console.log(firstInput, secondInput);
                  await AppgainSDK.initSDK(firstInput, secondInput, true);
                  Alert.alert('Success');
                  getTokenUpdateUserToken();
                } catch (error) {
                  const err = JSON.parse(error)
                  // console.log(err.message);
                  Alert.alert('Error: ', err.message);
                }
              }}
              title="Initialize"
            />
          </View>

          {/* <View style={styles.btn_cont}>
           <Prompt
               title="init user email,password,username"
               placeholder=""
               defaultValue=""
               numberOfTextIntput = {3}
               visible={isInitUserDialogVisible}
               onCancel={() => this.setInitUserTextShowDialog(false)}
               onSubmit={(email,password,username) => {
                 this.sendInitUserInput(email,password,username);
                 this.setInitUserTextShowDialog(false)
               }
               } />
             <Button
               id="initUserBtn"
               class="initUserBtn"
               accessibilityLabel="initUserBtn"
 
               style={styles.btn}
               onPress={async () => {
                 updateInitUserDialogVisible(true)
 
               }}
               title="Initialize User"
             />
 
           </View> */}
          <View style={styles.btn_cont}>
            <Button
              nativeID="matchLinkBtn"
              style={styles.btn}
              onPress={async () => {
                try {
                  await AppgainSDK.matchLink();

                  Alert.alert('Success');
                } catch (error) {
                  console.log('Error: ', error);
                  Alert.alert('Error: ', error);
                }
              }}
              title="Match Link"
            />
          </View>

          <View style={styles.btn_cont}>
            <Prompt
              title="fireAutomator triger point"
              placeholder=""
              defaultValue=""
              visible={isFireAutomatorDialogVisible}
              onCancel={() => this.fireAutomatorShowDialog(false)}
              onSubmit={inputText => {
                this.sendFireAutomatorInput(inputText);
                this.fireAutomatorShowDialog(false);
              }}
            />

            <Button
              nativeID="fireAutomatorBtn"
              style={styles.btn}
              onPress={async () => {
                updateIsFireAutomatorDialogVisible(true);
              }}
              title="Fire Automator"
            />
          </View>
          <View style={styles.btn_cont}>
            <Prompt
              title="fireAutomator triger point,name,value"
              placeholder=""
              defaultValue=""
              numberOfTextIntput={3}
              visible={isFireAutomatorWithPersonlaiztionDialogVisible}
              onCancel={() =>
                this.fireAutomatorWithPersonlaiztionShowDialog(false)
              }
              onSubmit={(inputText1, fireAutomatorKey, fireAutomatorValue) => {
                this.sendAutomatorWithProsInput(inputText1, 'key', 'appgain');
                this.fireAutomatorWithPersonlaiztionShowDialog(false);
              }}
            />

            <Button
              nativeID="fireAutomatorPersBtn"
              style={styles.btn}
              onPress={() =>
                updateIsFireAutomatorWithPersonlaiztionDialogVisible(true)
              }
              title="Fire Automator With Personalization"
            />
          </View>
          <View style={styles.btn_cont}>
            <Prompt
              title="Enter Purchase amount"
              placeholder="Name"
              defaultValue=""
              numberOfTextIntput={3}
              visible={isPurchaseDialogVisible}
              onCancel={() => this.purchaseShowDialog(false)}
              onSubmit={(productName, currency, amount) => {
                this.sendPurchaseInput(productName, currency, amount);
                this.purchaseShowDialog(false);
              }}
            />

            <Button
              nativeID="addPurchaseBtn"
              style={styles.btn}
              onPress={() => updateIsPurchaseDialogVisible(true)}
              title="Add Purchase"
            />
          </View>
          <View style={styles.btn_cont}>
            <Prompt
              title="Enter Email Input"
              placeholder=""
              defaultValue=""
              visible={isEmailDialogVisible}
              onCancel={() => this.emailTextShowDialog(false)}
              onSubmit={inputText3 => {
                this.sendEmailInput(inputText3);
                this.emailTextShowDialog(false);
              }}
            />

            <Button
              id="addEmailBtn"
              testID="addEmailBtn"
              nativeID="addEmailBtn"
              accessibilityLabel="addEmailBtn"
              style={styles.btn}
              onPress={() => updateIsEmailDialogVisible(true)}
              title="Add Email Channel"
            />
          </View>

          <View style={styles.btn_cont}>
            <Prompt
              id="smsid"
              testID="smsid"
              accessibilityLabel="smsid"
              title="Enter phone number Input"
              placeholder=""
              defaultValue=""
              visible={isSmsDialogVisible}
              onCancel={() => this.smsTextShowDialog(false)}
              onSubmit={inputText4 => {
                this.sendSmsInput(inputText4);
                this.smsTextShowDialog(false);
              }}
            />

            <Button
              nativeID="addSmsBtn"
              testID="addSmsBtn"
              style={styles.btn}
              onPress={async () => updateIsSmsDialogVisible(true)}
              title="Add SMS Channel"
            />
          </View>
          <View style={styles.btn_cont}>
            <Prompt
              title="Enter Log Event"
              placeholder="Type"
              defaultValue=""
              numberOfTextIntput={4}
              visible={isLogEventDialogVisible}
              onCancel={() => this.setLogEventTextShowDialog(false)}
              onSubmit={(
                logEventAction,
                logEventType,
                logEventExtrasKey,
                logEventExtrasValue,
              ) => {
                this.sendLogEventInput(
                  logEventAction,
                  logEventType,
                  logEventExtrasKey,
                  logEventExtrasValue,
                );
                this.setLogEventTextShowDialog(false);
              }}
            />
            <Button
              nativeID="logEventBtn"
              style={styles.btn}
              onPress={async () => {
                updateLogEventDialogVisible(true);
              }}
              title="Log Event"
            />
          </View>
          <View style={styles.btn_cont}>
            <Button
              class="getUserIdBtn"
              nativeID="getUserIdBtn"
              style={styles.btn}
              onPress={async () => {
                try {
                  let id = await AppgainSDK.getUserId();
                  let splitId = id.split(':');
                  let finalId = splitId[1].slice(0, -1);
                  Alert.alert('userid=' + finalId);
                } catch (error) {
                  console.log('Error: ', error);
                  Alert.alert('Error: ', error);
                }
              }}
              title="GET USER ID"
            />
          </View>
          {/* <View style={styles.btn_cont}>
           <Prompt
             
             title="Enter user id Input"
             placeholder=""
             defaultValue=""
             visible={isSetUserDialogVisible}
             onCancel={() => this.setUserIdTextShowDialog(false)}
             onSubmit={(inputText5) => {
               this.sendUserIdInput(inputText5)
               this.setUserIdTextShowDialog(false)
 
             }
 
             } />
             <Button
              
               nativeID = "getUserIdBtn"
               class="setUserBtn"
               style={styles.btn}
               onPress={async () => {
                 updateIsSetUserIdDialogVisible(true)
               }
               }
               title="SET USER ID"
             />
           </View> */}
          <View style={styles.btn_cont}>
            <Prompt
              title="Update userdata"
              placeholder="User email"
              defaultValue=""
              numberOfTextIntput={4}
              visible={isUpdatetUserDialogVisible}
              onCancel={() => this.setUpdateUserTextShowDialog(false)}
              onSubmit={(
                updateUserEmail,
                updateUserPhone,
                updateUserUpdatedField1,
                updateUserUpdatedField2,
              ) => {
                this.sendUpdateUserInput(
                  updateUserEmail,
                  updateUserPhone,
                  updateUserUpdatedField1,
                  updateUserUpdatedField2,
                );
                this.setUpdateUserTextShowDialog(false);
              }}
            />
            <Button
              nativeID="updateUserDataBtn"
              style={styles.btn}
              onPress={async () => {
                updateUpdateUserDialogVisible(true);
              }}
              title="UPDATE USER"
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  btn_cont: {
    height: 50,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'black',
  },
  btn: {
    backgroundColor: 'transparent',
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 10,
    paddingHorizontal: 10,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
